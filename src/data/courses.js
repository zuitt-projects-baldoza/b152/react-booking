let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description:"Consectetur pariatur laboris adipisicing ad pariatur id cupidatat incididunt culpa culpa sit enim.",
		price: 25000,
		onOffer:true
	},	
	{
		id: "wdc002",
		name: "Python-Django",
		description:"Tempor aliqua nisi deserunt cillum pariatur amet adipisicing nisi duis ut et et non aute ullamco.",
		price: 35000,
		onOffer:true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description:"Lorem ipsum duis minim amet commodo ut in duis esse nisi amet eu sit duis sunt.",
		price: 45000,
		onOffer:true
	},	
	{
		id: "wdc004",
		name: "Ruby on Rails",
		description:"Qui adipisicing excepteur fugiat excepteur reprehenderit ut sed ea sit officia quis pariatur.",
		price: 50000,
		onOffer:true
	}

];

export default coursesData;
