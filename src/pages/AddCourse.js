import {useState,useContext,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from "sweetalert2";

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function AddCourse(){
	const {user,setUser} = useContext(UserContext);

	const [name,setName]=useState("");
	const [description,setDescription]=useState("");
	const [price,setPrice]=useState("");


	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[name,description,price])

	function createCourse(e){

	e.preventDefault();

let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');

fetch('http://localhost:4000/course/',{

		method: 'POST',
		headers: {
			'Authorization': `Bearer ${token}`,
			"Content-Type": "application/json"},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
		})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		console.log(isAdmin);

		if(data._id){
			Swal.fire({
				icon: "success",
				title: "Course Creation Successful"
			})

			window.location.href = "/login";

		} else {
			Swal.fire({

				icon: "error",
				title: "Course Creation Failed.",
				text: data.message
			})

		}

			})
		}

	return (
			<>
			<h1 className="my-5 text-center">Create a Course</h1>
			<Form onSubmit={e => createCourse(e)}>
			<Form.Group>
					<Form.Label>
					Course name:
					</Form.Label>
					<Form.Control type="text" placeholder="Enter Course Name" required value={name} onChange={e => {setName(e.target.value)}}/>
			</Form.Group>	

			<Form.Group>
					<Form.Label>
					Description:
					</Form.Label>
					<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
			</Form.Group>	

			<Form.Group>
					<Form.Label>
					Price:
					</Form.Label>
					<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
			</Form.Group>
		
					{
					isActive

					? <Button variant="primary" type="submit">Submit</Button>
					: <Button variant="primary" disabled>Submit</Button>
				}

			</Form>
		</>

		)
}


