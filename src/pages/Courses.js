import {useState,useEffect,useContext} from 'react'
import Banner from '../components/Banner';
import CourseCard from '../components/Course';

import AdminDashboard from '../components/AdminDashboard';
//import coursesData from '../data/courses';

import UserContext from '../userContext'

export default function Courses(){

	const {user} = useContext(UserContext);
	//console.log(user);

//console.log(coursesData)

	let coursesBanner = {

		title: "Welcome to the Courses Page.",
		description: "View one of our courses below.",
		buttonText: "Register/Login to Enroll",
		destination: "/register"

	}

	// let propCourse1 = "sample data 1";
	// let propCourse2 = "sample data 2";
	// let propCourse3 = "sample data 3";
const [coursesArray,setCoursesArray] = useState([])

useEffect(()=>{

	fetch("http://localhost:4000/course/getAllActiveCourse")
	.then(res=>res.json())
	.then(data=>{
		//console.log(data)

		setCoursesArray(data.map(course=>{
			return(
				<CourseCard key={course._id} courseProp={course} />
				)
		}))
	})
},[])

// 	let coursesComponents = coursesData.map(course=>{
// 		return <CourseCard courseProp={course} key={course.id}/>
// 	})

// 	console.log(coursesComponents)

	return (

		user.isAdmin
		?
		<AdminDashboard />
		:
		<>
			<Banner bannerProp={coursesBanner}/>
			{coursesArray}
			{/*{coursesComponents}*/}
		</>
		)
}






