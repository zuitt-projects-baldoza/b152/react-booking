//Our home page component will be the parent component of our banner and highlights components.

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){
	
	let sampleProp = "I am sample data passed from Home component to Banner Component."

	let sampleProp2 = "This sample data is passed from our Home component to our Highlights component"

	let bannerData = {
		title: "Zuitt Booking System B152",
		description: "View and book a course from our catalog!",
		buttonText:"View Our Courses",
		destination: "/courses"
	}

	return (
		<>
		<Banner bannerProp={bannerData}/>
		<Highlights highlightsProp={sampleProp2}/>
		</>
		)
}