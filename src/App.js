import {useState,useEffect} from 'react';

import {Container} from 'react-bootstrap'

//import react-router-dom components to simulate/implement page routing in our app.
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'

// import Banner from './components/Banner'
import AppNavBar from './components/AppNavBar'
// import About from './components/About'
// import Highlights from './components/Highlights'
import Home from './pages/Home';
//import Course from './components/Course'
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import AddCourse from './pages/AddCourse';
import Logout from './pages/Logout';
import ViewCourse from './pages/ViewCourse';

import { UserProvider } from './userContext';

import './App.css';

// import React from 'react';
export default function App(){

const [user,setUser] = useState({

  id:null,
  isAdmin:null

})

useEffect(()=>{

  //fetch() - to get userDetails whenever page refresh and main component initially renders.
  fetch('http://localhost:4000/users/getUserDetails',{

    method:'GET',
    headers: {

      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }

  })
  .then(res => res.json())
  .then(data => {
    //console.log(data);
    setUser({

      id: data._id,
      isAdmin: data.isAdmin

    })
  })

},[])

//console.log(user);

  const unsetUser = () => {

    localStorage.clear()

  }

  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}} >
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/viewCourse/:courseId" element={<ViewCourse />} /> 
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/addCourse" element={<AddCourse />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>
      </Router>
      </UserProvider>
    </>
  )
}