import {useState} from 'react'
import {Button,Card} from 'react-bootstrap';

//import link component 
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
	
	//console.log(courseProp)
	//console.log("Hello, I will run whenever we update a state with its setter function.")
	const [count,setCount] = useState(0);
	const [seats,setSeats] = useState(30);
	//console.log(count);
	//console.log(useState(0));

	//let seat=0;

	function enroll(){
		setCount(count+1);
		//seat++;
		setSeats(seats-1);
		
	}

	//console.log(courseProp)

	return (
		<Card>
			<Card.Body>
				<Card.Title>
					{courseProp.name}
				</Card.Title>
			<Card.Text>
				{courseProp.description}
			</Card.Text>
			<Card.Text>
				Price: {courseProp.price}
			</Card.Text>
			<Card.Text>
				<Link to={`/courses/viewCourse/${courseProp._id}`}className="btn btn-primary">View Course</Link>
			</Card.Text>
			</Card.Body>
		</Card>
		)
}

