import {Row, Col, Card, Class} from 'react-bootstrap'

export default function About(){

	return (
	<Card bg="warning" className="text-justify">
	<Row>
	<Col className="p-5">
		<class bg="info">
		<h1 className="my-5">About Me</h1>
		</class>
		<h2 className="mt-3">Bianca Baldoza</h2>
		<h3>Full Stack Web Developer</h3>
		<p className="mb-4">I love to code!</p>
		<h4>Contact Me</h4>
		<ul>
		<li>Email: biancaabaldoza@gmail.com</li>
		<li>Mobile: 09 123 456 789</li>
		<li>Address: Somewhere over the rainbow</li>
		</ul>
	</Col>
	</Row>
	</Card>
	)
}