import {Row,Col,Card} from 'react-bootstrap';

export default function Highlights({highlightsProp}){
	console.log(highlightsProp);
	
	return (

		<Row className="my-3">
			<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
					<Card.Body>
					<Card.Title>
						<h2>Learn From Home</h2>
					</Card.Title>
					<Card.Text>
						Cupidatat sit aliquip aliqua eiusmod sint sed irure adipisicing aliquip amet et proident minim sit et in in.
					</Card.Text>
					</Card.Body>
				</Card>
				</Col>
					<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
					<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum fugiat magna ea dolore ullamco nisi laborum velit commodo exercitation laborum tempor reprehenderit elit velit dolor duis deserunt eu nisi mollit id eu ea duis.
					</Card.Text>
					</Card.Body>
				</Card>
				</Col>
				
					<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
					<Card.Body>
					<Card.Title>
						<h2>Be Part of Our Community</h2>
					</Card.Title>
					<Card.Text>
						Ut sint ullamco non ullamco ut in aliqua dolore ut laboris veniam consequat pariatur aute ad laborum irure ullamco mollit amet reprehenderit do aute minim laboris est id non nostrud tempor amet velit dolor sit.
					</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}